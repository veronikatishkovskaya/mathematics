import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var firstNumber: UITextField!
    @IBOutlet weak var secondNumber: UITextField!
    @IBOutlet weak var thirdNumber: UITextField!
    @IBOutlet weak var discriminantResult: UILabel!
    @IBOutlet weak var firstRoot: UILabel!
    @IBOutlet weak var secondRoot: UILabel!
    @IBOutlet weak var equationLabel: UILabel!
    @IBOutlet weak var discriminantDescription: UILabel!
    @IBOutlet weak var alertLabel: UILabel!
    
    var firstOperand = Int()
    var secondOperand = Int()
    var thirdOperand = Int()
    var textFieldArray = [UITextField]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        discriminantDescription.lineBreakMode = .byTruncatingTail
        discriminantDescription.numberOfLines = 0
        discriminantDescription.textAlignment = .left
        alertLabel.lineBreakMode = .byTruncatingTail
        alertLabel.numberOfLines = 0
        
    }
    
    @IBAction func calculateResult(_ sender: Any) {
        guard let firstNumberText = firstNumber.text else { return }
        guard let secondNumberText = secondNumber.text else { return }
        guard let thirdNumberText = thirdNumber.text else { return }
        
        textFieldArray.append(firstNumber)
        textFieldArray.append(secondNumber)
        textFieldArray.append(thirdNumber)
        
        if firstNumber.text == "0" {
            alertLabel.text = "Коэффициент при первом слагаемом уравнения не может быть равным нулю"
            firstNumber.layer.borderWidth = 2
            firstNumber.layer.borderColor = CGColor(red: 255, green: 0, blue: 0, alpha: 1)
        } else if isValidText(string: firstNumberText) && isValidText(string: secondNumberText) && isValidText(string: thirdNumberText) {
            alertLabel.text = ""
            firstNumber.layer.borderWidth = 0
            secondNumber.layer.borderWidth = 0
            thirdNumber.layer.borderWidth = 0
            calculateDiscriminantAndRoots()
            equationLabel.text = "\(firstOperand)х² + \(secondOperand)x + \(thirdOperand) = 0"
        } else {
            firstNumber.layer.borderWidth = 0
            secondNumber.layer.borderWidth = 0
            thirdNumber.layer.borderWidth = 0
            for textField in textFieldArray {
                guard let text = textField.text else { return }
                if text.isInt == false {
                    textField.layer.borderWidth = 2
                    textField.layer.borderColor = CGColor(red: 255, green: 0, blue: 0, alpha: 1)
                    alertLabel.text = "Вы можете ввести только числа"
                } else {
                    alertLabel.text = "Вы можете ввести только числа"
                }
            }
        }
    }
    
    @IBAction func deleteCalculations(_ sender: UIButton) {
        firstNumber.text = ""
        secondNumber.text = ""
        thirdNumber.text = ""
        discriminantResult.text = ""
        firstRoot.text = ""
        secondRoot.text = ""
        equationLabel.text = ""
        discriminantDescription.text = ""
        
    }
    
    func calculateDiscriminantAndRoots() {
        guard let xSquared = firstNumber.text else {return}
        firstOperand = Int(xSquared) ?? 1
        
        guard let xNumber = secondNumber.text else {return}
        secondOperand = Int(xNumber) ?? 1
        
        guard let number = thirdNumber.text else {return}
        thirdOperand = Int(number) ?? 0
        
        let discriminant = secondOperand * secondOperand - 4 * firstOperand * thirdOperand
        discriminantResult.text = "\(discriminant)"
        
        let negativeNumber = -secondOperand
        
        if discriminant > 0 {
            let rootOne = (Double(negativeNumber) + sqrt(Double(discriminant))) / (2 * Double(firstOperand))
            let rootTwo = (Double(negativeNumber) - sqrt(Double(discriminant))) / (2 * Double(firstOperand))
            firstRoot.text = "x1 = \(rootOne)"
            secondRoot.text = "x2 = \(rootTwo)"
            discriminantDescription.text = "Так как дискриминант больше нуля то, квадратное уравнение имеет два действительных корня:"
        } else if discriminant == 0 {
            let root = negativeNumber / 2 * firstOperand
            firstRoot.text = "x = \(root)"
            secondRoot.text = ""
            discriminantDescription.text = "Так как дискриминант равен нулю то, квадратное уравнение имеет один действительный корень:"
        } else if discriminant < 0 {
            firstRoot.text = ""
            secondRoot.text = ""
            discriminantDescription.text = "Так как дискриминант меньше нуля, то уравнение не имеет действительных решений."
        }
    }
    
    func isValidText(string: String) -> Bool {
        return string.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}

extension String {
    var isInt: Bool {
        return Int(self) != nil
    }
}

